package com.example.pso3;

import android.app.Notification;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    BroadcastReceiver receiver;
    private NotificationUtils mNotificationUtils;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mNotificationUtils = new NotificationUtils(this);
        Button createNotification = findViewById(R.id.start);
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(Intent.ACTION_POWER_CONNECTED)) {
                    Toast.makeText(context, "Telefon został podpięty do ładowania", Toast.LENGTH_SHORT).show();
                    addNotification();
                } else {
                    intent.getAction().equals(Intent.ACTION_POWER_DISCONNECTED);
                    Toast.makeText(context, "Telefon został odpięty od ładowania", Toast.LENGTH_SHORT).show();
                    addNotification();
                }
            }
        };
        createNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNotification();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter ifilter = new IntentFilter();
        ifilter.addAction(Intent.ACTION_POWER_CONNECTED);
        ifilter.addAction(Intent.ACTION_POWER_DISCONNECTED);
        registerReceiver(receiver, ifilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void addNotification() {
        GPSTracker gps = new GPSTracker(this);
        if (gps.canGetLocation()) {
            String location = gps.getLatitude() + ", " + gps.getLongitude();
            Notification.Builder builder = mNotificationUtils.getAndroidChannelNotification("Lokalizacja", location);
            mNotificationUtils.getManager().notify(101, builder.build());
        }
    }

}
